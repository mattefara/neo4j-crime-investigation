MATCH (:Time {year:2021, month: 2})<-[:HAPPEN_IN]-(c:Case)-[:TYPE_OF]->(t:Type)
RETURN t.name AS `Crimes`, count(DISTINCT c) AS Occurrences
ORDER BY Occurrences 
DESC LIMIT 5