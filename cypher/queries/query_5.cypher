MATCH (c1:Case)-[:AT]->(l:Place), (c2:Case)-[:AT]->(l:Place)
WHERE ID(c1) < ID(c2)
WITH c1, c2, l
RETURN DISTINCT c1.id, c2.id, l.name as `Location name`
LIMIT 5