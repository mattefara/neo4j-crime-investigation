MATCH (:Time {year:2021, month: 3})<-[:HAPPEN_IN]-(c:Case)
RETURN c.outcome as `Status`,  count(DISTINCT c) AS Occurrences
ORDER BY Occurrences DESC
LIMIT 5