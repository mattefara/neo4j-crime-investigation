MATCH (time:Time {year:2021, month: 4})<-[:HAPPEN_IN]-(crime:Case)-[:TYPE_OF]->(type:Type)
WHERE crime.outcome = "Investigation complete; no suspect identified"
WITH time, count(crime) as `Unsolved crimes`, type
MATCH (time)<-[:HAPPEN_IN]-()-[:TYPE_OF]->(type)
WITH DISTINCT type, `Unsolved crimes`, count(*) AS `Total crimes`
RETURN type.name AS `Crime type`, `Unsolved crimes`, `Total crimes`, 100 - `Unsolved crimes` /toFloat(`Total crimes`) * 100 as `Solved Percentage`
ORDER BY `Solved Percentage` DESC
LIMIT 15