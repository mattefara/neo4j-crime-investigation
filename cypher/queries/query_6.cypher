MATCH (t:Type {name: 'Drugs'})<-[:TYPE_OF]-(c1:Case)-[p1:AT]->(l1:Place), (t)<-[:TYPE_OF]-(c2:Case)-[p2:AT]->(l2:Place)
WHERE ID(c1) < ID(c2) AND point.distance(p1.position, p2.position) < 500
RETURN DISTINCT l1.name AS `Location 1`, l2.name as `Location 2`, point.distance(p1.position, p2.position) as Distance
ORDER BY Distance DESC, l1.name ASC
LIMIT 5