MATCH (:Time {year: 2021, month: 1})<-[:HAPPEN_IN]-(c:Case)-[:AT]->(l:Place)
RETURN l.name AS `Places`, count(DISTINCT c) AS Occurrences
ORDER BY Occurrences
DESC LIMIT 5