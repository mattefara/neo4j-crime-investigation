LOAD CSV WITH HEADERS FROM 
    "https://docs.google.com/spreadsheets/d/e/2PACX-1vTbvWmmKUYWqhJ2Kn_8ezr2iYnRGUT0bcHx0Gh_YnLcvObr1VI2qq4EATvLW3uQxHGhsW9vK4ACNl1B/pub?gid=2036635946&single=true&output=csv" as crimecsv
WITH crimecsv
MERGE (case:Case {
    id: crimecsv.crime_id, 
    reported_by: crimecsv.reported_by, 
    falls_within: crimecsv.falls_within,
    outcome: crimecsv.last_outcome_category
})
MERGE (type:Type {
    name: crimecsv.crime_type
})
MERGE (place:Place {
    name: crimecsv.location
})
MERGE (lsoa:LSOA {
    name: crimecsv.lsoa_name, 
    code: crimecsv.lsoa_code
})
MERGE (time:Time {
    month: toInteger(crimecsv.month), 
    year: toInteger(crimecsv.year)
})

CREATE (lsoa)-[:RESPOND]->(case),
(case)-[:TYPE_OF]->(type),
(case)-[:AT {
    position: point({
        latitude: toFloat(crimecsv.latitude), 
        longitude: toFloat(crimecsv.longitude)
    })
}]->(place),
(case)-[:HAPPEN_IN]->(time)