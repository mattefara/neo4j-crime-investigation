CREATE CONSTRAINT type_name_constraint IF NOT EXISTS
FOR (type:Type)
REQUIRE type.name IS UNIQUE