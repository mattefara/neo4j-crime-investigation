CREATE CONSTRAINT place_name_constraint IF NOT EXISTS
FOR (place:Place)
REQUIRE place.name IS UNIQUE