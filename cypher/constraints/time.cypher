CREATE CONSTRAINT time_constraint IF NOT EXISTS
FOR (time:Time)
REQUIRE (time.month, time.year) IS UNIQUE