CREATE CONSTRAINT lsoa_constraint IF NOT EXISTS
FOR (lsoa:LSOA)
REQUIRE (lsoa.name, lsoa.code) IS UNIQUE