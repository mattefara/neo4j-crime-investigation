CREATE POINT INDEX point_crime_position_index 
FOR () -[p:AT]-> ()
ON (p.position)
OPTIONS {
    indexConfig: {
        `spatial.cartesian.min`: [-100.0, -100.0], 
        `spatial.cartesian.max`: [100.0, 100.0]
    }
}