from neo4j import GraphDatabase


def run(tx, prop):
    query = "MATCH ... WHERE a.property = $prop"
    return list(tx.run(query, prop=prop))


driver = GraphDatabase.driver(URL, auth=(USERNAME,PASSWORD))
with driver.session() as session:
    session.read_transaction(run, prop='test')