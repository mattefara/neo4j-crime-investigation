from neo4j import GraphDatabase


def run(tx):
    query = "MATCH ..."
    return list(tx.run(query))


driver = GraphDatabase.driver(URL, auth=(USERNAME,PASSWORD))
with driver.session() as session:
    session.read_transaction(run)